//import 'package:calendar_app/Agendamento.dart';
import 'package:calendar_app/fancy_fab.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:getflutter/getflutter.dart';
//import 'package:flutter_boom_menu/flutter_boom_menu.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomePage(title: 'TopBar Gradient'),
    );
  }
}

class HomePage extends StatefulWidget {
  HomePage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size(double.infinity, 100),
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: 100,
            child: ClipPath(
              clipper: WaveClipperOne(),
              child: Container(
                child: GradientAppBar(
                  gradient: LinearGradient(colors: [
                    Colors.red,
                    Colors.purpleAccent,
                    Colors.blueAccent
                  ]),
                  title: Text(widget.title),
                ),
              ),
            ),
          ),
        ),
        body: _body(),
        floatingActionButton: FancyFab(),
      ),
    );
  }

  _body() {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.all(10.0),
        child: Center(
          child: Column(
            children: <Widget>[
              Card(margin:
                    EdgeInsets.only(left: 0, right: 0, top: 1.0, bottom: 1.0),
                color: Colors.grey[130],
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(40), bottomRight: Radius.circular(40)),
                ),
                child: Container(
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(topLeft: Radius.circular(40), bottomRight: Radius.circular(40)),
                      border: Border.all(color: Colors.purple, width: 2)
                  ),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text("Bronzeamento Natural",
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(fontSize: 20, height: 2, fontWeight: FontWeight.bold),
                        ),
                        Text("Dia 16 de Março",
                          style: TextStyle(fontSize: 15),
                        ),
                        ButtonTheme(
                          child: ButtonBar(
                            children: <Widget>[
                              GFButton(
                                onPressed: () {},
                                text: "DETALHES",
                                shape: GFButtonShape.pills,
                                type: GFButtonType.solid,
                                color: GFColors.INFO,
                              ),
                              GFButton(
                                onPressed: () {},
                                text: "CANCELAR",
                                shape: GFButtonShape.pills,
                                type: GFButtonType.solid,
                                color: GFColors.DANGER,
                              ),
                            ],
                          ),
                        ),
                      ]
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
